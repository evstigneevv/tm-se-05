package ru.evstigneev.tm.command;

public class HelpCommand extends AbstractCommand {

    @Override
    public final String command() {
        return "HELP";
    }

    @Override
    public final String description() {
        return "Show available commands and their description";
    }

    @Override
    public void execute() throws Exception {
        for (AbstractCommand command : bootstrap.getCommands().values()) {
            System.out.println(command.command() + " " + command.description());
        }
    }

}
