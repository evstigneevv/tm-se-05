package ru.evstigneev.tm.command;

import ru.evstigneev.tm.entity.Task;

public class TaskGetOneCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SOT";
    }

    @Override
    public String description() {
        return "Shows specify task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("input task ID: ");
        String input = bootstrap.getScanner().nextLine();
        if (bootstrap.getTaskService().findOne(input) == null) {
            return;
        }
        Task t = bootstrap.getTaskService().findOne(input);
        System.out.println("|=====================================|=====================================|=====================================|");
        System.out.println("|          project ID                 |            task ID                  |               task name             |");
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        System.out.println("|" + t.getProjectId() + " |" + t.getId() + " | " + t.getName());
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
    }

}
