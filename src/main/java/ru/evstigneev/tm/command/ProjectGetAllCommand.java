package ru.evstigneev.tm.command;

import ru.evstigneev.tm.entity.Project;

public class ProjectGetAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SAP";
    }

    @Override
    public String description() {
        return "Shows all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("|=====================================|=====================================|");
        System.out.println("|          project ID                 |            project name             |");
        System.out.println("|_____________________________________|_____________________________________|");
        for (Project p : bootstrap.getProjectService().findAll().values()) {
            System.out.println("|" + p.getId() + " | " + p.getName());
            System.out.println("|_____________________________________|_____________________________________|");
        }
    }

}
