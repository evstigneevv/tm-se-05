package ru.evstigneev.tm.command;

import ru.evstigneev.tm.bootstrap.Bootstrap;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

}