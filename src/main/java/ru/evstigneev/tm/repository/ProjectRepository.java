package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.entity.Project;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class ProjectRepository {

    private Map<String, Project> projectMap = new LinkedHashMap<>();

    public Project create(final String projectName) {
        Project project = new Project(projectName, UUID.randomUUID().toString());
        projectMap.put(project.getId(), project);
        return project;
    }

    public boolean remove(final String projectId) {
        return projectMap.remove(projectId) != null;
    }

    public Project update(final String projectId, final String projectName) {
        projectMap.get(projectId).setName(projectName);
        return projectMap.get(projectId);
    }

    public Map<String, Project> findAll() {
        return projectMap;
    }

    public Project findOne(final String projectId) {
        return projectMap.get(projectId);
    }

    public Project merge(final Project project) {
        return projectMap.put(project.getId(), project);
    }

    public Project persist(final Project project) {
        return projectMap.put(project.getId(), project);
    }

    public void removeAll() {
        projectMap.clear();
    }

}
