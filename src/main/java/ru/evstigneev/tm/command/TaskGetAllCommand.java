package ru.evstigneev.tm.command;

import ru.evstigneev.tm.entity.Task;

public class TaskGetAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SAT";
    }

    @Override
    public String description() {
        return "Shows all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("|=====================================|=====================================|=====================================|");
        System.out.println("|          project ID                 |            task ID                  |               task name             |");
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        for (Task t : bootstrap.getTaskService().findAll().values()) {
            System.out.println("|" + t.getProjectId() + " |" + t.getId() + " | " + t.getName());
            System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        }
    }

}
