package ru.evstigneev.tm.command;

public class TaskUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "TU";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("enter task ID");
        String input = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().update(input, bootstrap.getScanner().nextLine());
    }

}
