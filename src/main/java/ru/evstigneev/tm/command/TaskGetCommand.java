package ru.evstigneev.tm.command;

import ru.evstigneev.tm.entity.Task;

import java.util.Collection;

public class TaskGetCommand extends AbstractCommand {

    @Override
    public String command() {
        return "ST";
    }

    @Override
    public String description() {
        return "Shows all tasks for specific project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("input project ID: ");
        Collection<Task> taskList = bootstrap.getTaskService().getTaskListByProjectId(bootstrap.getScanner().nextLine());
        System.out.println("|=====================================|=====================================|=====================================|");
        System.out.println("|          project ID                 |            task ID                  |               task name             |");
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        for (Task t : taskList) {
            System.out.println("|" + t.getProjectId() + " |" + t.getId() + " | " + t.getName());
            System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        }
    }

}
