package ru.evstigneev.tm.bootstrap;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.TaskService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Bootstrap {

    private final Scanner scanner = new Scanner(System.in);
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectService projectService = new ProjectService(new ProjectRepository(), taskRepository);
    private final TaskService taskService = new TaskService(taskRepository);
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init(final Class... classes) {
        if (classes != null) {
            for (Class clazz : classes) {
                registry(clazz);
            }
        }
    }

    private void registry(final Class clazz) {
        if (clazz.getSuperclass().equals(AbstractCommand.class)) {
            try {
                final AbstractCommand command = (AbstractCommand) clazz.newInstance();
                command.setBootstrap(this);
                registry(command);
            } catch (InstantiationException | IllegalAccessException | CommandCorruptException e) {
                e.printStackTrace();
            }
        }
    }

    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final String commandText = command.command();
        final String description = command.description();
        if (commandText == null || description == null || commandText.isEmpty() || description.isEmpty())
            throw new CommandCorruptException();
        commands.put(commandText, command);
    }

    public void start() throws Exception {
        System.out.println("Welcome to Task Manager!");
        System.out.print("input command or \"EXIT\" to exit...");
        while (true) {
            String command = scanner.nextLine().toUpperCase();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            throw new EmptyStringException();
        }
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            throw new CommandCorruptException();
        }
        abstractCommand.execute();
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

}
