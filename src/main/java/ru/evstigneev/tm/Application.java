package ru.evstigneev.tm;

import ru.evstigneev.tm.bootstrap.Bootstrap;
import ru.evstigneev.tm.command.*;

public class Application {

    private static final Class[] classes = {HelpCommand.class, ProjectCreateCommand.class,
            ProjectDeleteCommand.class, ProjectGetAllCommand.class, ProjectUpdateCommand.class, TaskCreateCommand.class,
            TaskUpdateCommand.class, TaskDeleteCommand.class, TaskGetCommand.class, TaskGetOneCommand.class,
            TaskGetAllCommand.class, TaskDeleteAllCommand.class, ProjectDeleteAllCommand.class, ExitCommand.class};

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(classes);
        bootstrap.start();
    }

}