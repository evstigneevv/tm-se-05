package ru.evstigneev.tm.exception;

public class CommandCorruptException extends Exception {

    public CommandCorruptException() {
        super("Command is corrupted!");
    }

}
