package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private Map<String, Task> taskMap = new LinkedHashMap<>();

    public Task create(final String taskName, final String projectId) {
        Task task = new Task(taskName, UUID.randomUUID().toString(), projectId);
        return taskMap.put(task.getId(), task);
    }

    public boolean remove(final String taskId) {
        return taskMap.remove(taskId) != null;
    }

    public Task update(final String taskId, final String taskName) {
        taskMap.get(taskId).setName(taskName);
        return taskMap.get(taskId);
    }

    public Collection<Task> getTaskListByProjectId(final String projectId) {
        List<Task> taskListByProjectId = new ArrayList<>();
        Collection<Task> taskList = taskMap.values();
        for (Task t : taskList) {
            if (t.getProjectId().equals(projectId)) {
                taskListByProjectId.add(t);
            }
        }
        return taskListByProjectId;
    }

    public Map<String, Task> findAll() {
        return taskMap;
    }

    public boolean deleteAllProjectTasks(final String projectId) {
        boolean isDeleted = false;
        Collection<Task> projectTasks = taskMap.values();
        for (Task t : projectTasks) {
            if (t.getProjectId().equals(projectId)) {
                taskMap.remove(t.getId());
                isDeleted = true;
            }
        }
        return isDeleted;
    }

    public Task findOne(final String taskId) {
        return taskMap.get(taskId);
    }

    public void removeAll() {
        taskMap.clear();
    }

    public Task merge(final Task task) {
        if (findOne(task.getId()) != null) {
            taskMap.put(task.getId(), task);
        }
        return persist(task);
    }

    public Task persist(final Task task) {
        return taskMap.put(task.getId(), task);
    }

}