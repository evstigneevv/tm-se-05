package ru.evstigneev.tm.command;

public class ProjectDeleteAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DAP";
    }

    @Override
    public String description() {
        return "Delete all projects!";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getProjectService().removeAll();
        System.out.println("All projects deleted!");
    }

}
