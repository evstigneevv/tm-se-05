package ru.evstigneev.tm.command;

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UP";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("input project ID");
        System.out.println("input project new name");
        String input = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().update(input, bootstrap.getScanner().nextLine());
    }

}
