package ru.evstigneev.tm.command;

public class TaskDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DT";
    }

    @Override
    public String description() {
        return "Delete task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter task ID: ");
        if (bootstrap.getTaskService().remove(bootstrap.getScanner().nextLine())) {
            System.out.println("Task was deleted!");
        } else {
            System.out.println("Task wasn't deleted!");
        }
    }

}
