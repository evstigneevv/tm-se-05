package ru.evstigneev.tm.command;

public class TaskDeleteAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DAT";
    }

    @Override
    public String description() {
        return "Delete all tasks";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getTaskService().removeAll();
        System.out.println("All tasks deleted!");
    }

}
