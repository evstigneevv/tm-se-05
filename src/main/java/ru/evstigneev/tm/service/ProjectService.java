package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;

import java.util.Map;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Map<String, Project> findAll() throws Exception {
        if (projectRepository.findAll().isEmpty()) {
            throw new RepositoryException("No projects found!");
        }
        return projectRepository.findAll();
    }

    public Project create(final String projectName) throws EmptyStringException {
        if (projectName == null || projectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.create(projectName);
    }

    public boolean remove(final String projectId) throws EmptyStringException {
        if (projectId == null || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        taskRepository.deleteAllProjectTasks(projectId);
        return projectRepository.remove(projectId);
    }

    public Project update(final String projectId, final String newProjectName) throws EmptyStringException {
        if (projectId == null || newProjectName == null || projectId.isEmpty() || newProjectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.update(projectId, newProjectName);
    }

    public Project findOne(final String projectId) throws EmptyStringException {
        if (projectId == null || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.findOne(projectId);
    }

    public Project merge(final Project project) throws Exception {
        if (project == null) {
            throw new RepositoryException("No project!");
        }
        return projectRepository.merge(project);
    }

    public Project persist(final Project project) throws Exception {
        if (project == null) {
            throw new RepositoryException("No project!");
        }
        return projectRepository.persist(project);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

}
