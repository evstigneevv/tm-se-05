package ru.evstigneev.tm.command;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "CT";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getProjectService().findAll();
        System.out.println("input project ID: ");
        String input = bootstrap.getScanner().nextLine();
        if (bootstrap.getProjectService().findOne(input) != null) {
            System.out.println("input new task name into project: ");
            bootstrap.getTaskService().create(bootstrap.getScanner().nextLine(), input);
            System.out.println("task created!");
        } else {
            System.out.println("Project doesn't exist!");
        }
    }

}
