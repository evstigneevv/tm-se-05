package ru.evstigneev.tm.command;

public class ProjectDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DP";
    }

    @Override
    public String description() {
        return "Delete specify project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Input project ID: ");
        String input = bootstrap.getScanner().nextLine();
        if (bootstrap.getProjectService().remove(input)) {
            System.out.println("Project was deleted!");
        } else {
            System.out.println("Project wasn't deleted!");
        }
    }

}
