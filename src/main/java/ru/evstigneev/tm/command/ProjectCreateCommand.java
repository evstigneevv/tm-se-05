package ru.evstigneev.tm.command;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public final String command() {
        return "CP";
    }

    @Override
    public final String description() {
        return "Create a new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.print("input new project name: ");
        bootstrap.getProjectService().create(bootstrap.getScanner().nextLine());
        System.out.println("project created!");
    }

}
