package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;
import ru.evstigneev.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.Map;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String taskName, final String projectId) throws EmptyStringException {
        if (taskName == null || projectId == null || taskName.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.create(taskName, projectId);
    }

    public Map<String, Task> findAll() {
        return taskRepository.findAll();
    }

    public boolean remove(final String taskId) throws EmptyStringException {
        if (taskId == null || taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.remove(taskId);
    }

    public Collection<Task> getTaskListByProjectId(final String projectId) throws EmptyStringException {
        if (projectId == null || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.getTaskListByProjectId(projectId);
    }

    public Task update(final String taskId, final String taskName) throws EmptyStringException {
        if (taskId == null || taskName == null || taskId.isEmpty() || taskName.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.update(taskId, taskName);
    }

    public Task findOne(final String taskId) throws EmptyStringException {
        if (taskId == null || taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.findOne(taskId);
    }

    public Task merge(final Task task) throws Exception {
        if (task == null) {
            throw new RepositoryException("No project!");
        }
        return taskRepository.merge(task);
    }

    public Task persist(final Task task) throws Exception {
        throw new RepositoryException("No project!");
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

}
